from collections import Iterable


class OntologyNode():
    """
    the level of root node is 0.
    
    """

    def __init__(self, value):
        self.value = value
        self.children = []
        self.parent = None

    def get_value(self):
        return self.value

    def set_value(self, v):
        self.value = v

    def is_leaf(self):
        tag = False
        if len(self.children) == 0:
            tag = True
        return tag

    def set_parent(self, parent):
        self.parent = parent

    def get_parent(self):
        return self.parent

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        """
        get the list of children (node)
        """
        return self.children

    def get_leafnodes(self):
        """
        return a list of leaf values
        """

        result = []

        if self.is_leaf():
            result.append(self.value)

        else:
            children = self.get_children()
            for child in children:
                result.append(child.get_leafnodes())
        return list(flatten(result))  ###return values

    def get_level(self):
        """
        get the level of current node. Root node is 0.
        """
        if self.parent is None:
            return 0
        else:
            return self.parent.get_level() + 1

    def get_root_value(self):
        if self.parent:
            return self.value
        else:
            up = self.get_parent()
            return up.get_root_value()

    def __str__(self):
        r = ['value: ', self.value, 'children: ', self.children]
        return ' '.join(r)


def value_to_node(root, value):
    """
    input value, return the corresponding node object, otherwise return none
    
    Args:
        root: the root node of the tree
        value: the value of the node we are looking for
    Return:
        the node object or none.
    """
    if root.value == value:
        return root
    else:
        childrenList = root.get_children()
        for child in childrenList:
            tmp = value_to_node(child, value)
            if tmp is not None:
                return tmp


def get_ancestors(root, node):
    """
    given the node, return its all ancestors
    传入node， 得到所有祖先的value list
    
    Args:
        root
        node
    Return:
        a list of ancestor values of this node
    """
    ancestorList = []

    def get_ancestor_help(root, node, ancestorList):
        if node.get_parent() is None:
            return ancestorList
        else:
            currentParent = node.get_parent()
            ancestorList.append(currentParent.get_value())
            get_ancestor_help(root, currentParent, ancestorList)
            return ancestorList

    return get_ancestor_help(root, node, ancestorList)


def get_all_leafnodes(root, node):
    """
    given the node, return its all leaf node (in the list of value form)
    如果输入的node是 leaf, 那么就直接将该node 输出
    """
    childrenList = []

    # help function to maintain the list 
    def get_all_leaf_help(root, node, childrenList):
        if node.is_leaf():
            childrenList.append(node.get_value())
            return childrenList
        else:
            for child in node.get_children():
                #                 childrenList.append(child.getValue())
                get_all_leaf_help(root, child, childrenList)
            return childrenList

    return list(flatten(get_all_leaf_help(root, node, childrenList)))


def get_all_descendants(root, node):
    """
    given the node, return its all children (not only leaf node, but all children till leaf)
    如果输入的 node 是leaf, 那么输出就是 none
    """
    childrenList = []

    def get_all_children_help(root, node, childrenList):
        """
        如果没有孩子, 就不做操作;
        如果有孩子, 就把孩子加进去, 再递归
        """
        if node.get_children() is None:
            return childrenList
        else:
            for child in node.get_children():
                childrenList.append(child.get_value())
                get_all_children_help(root, child, childrenList)
            return childrenList

    return get_all_children_help(root, node, childrenList)


def get_node_by_level(root, node, level):
    """
    given the root, node, and level, return the node of the specified level
    if the level is out of the ground node level, then it returns leaf node
    """
    current_level = node.get_level()
    # root is level 0
    level_difference = current_level - level
    for i in range(level_difference):
        node = node.get_parent()
    return node


def flatten(items):
    """Yield items from any nested iterable; 
    >>> l = [1, 2, 3, [3, 4, 5, [7, 8]]]
    >>> f = list(flatten(l))
     [1, 2, 3, 3, 4, 5, 7, 8]
    """
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x
