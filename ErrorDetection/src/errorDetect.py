import csv
from src.OntologyNode import OntologyNode
from src.OntologyTree import OntologyTree
from src.OntologyNode import getFamily,findNodes
# csv_reader = csv.reader(open('../data/test1.csv', encoding='utf-8'))
#
# dataMat=[]
#
# for row in csv_reader:
#     dataMat.append(row)

a = OntologyNode('North America')
b = OntologyNode('Canada')
c = OntologyNode('America')
d = OntologyNode('ON')
e = OntologyNode('BC')
f = OntologyNode('Hamilton')
g = OntologyNode('Toronto')
h = OntologyNode('Vancouver')
i = OntologyNode('Kamloops')
j = OntologyNode('Penticton')
k = OntologyNode('NB')
l = OntologyNode('Baker Brook')
m = OntologyNode('Bathurst')
n = OntologyNode('Dieppe')

root=a

b.setParent(a)
c.setParent(a)
d.setParent(b)
e.setParent(b)
k.setParent(b)
f.setParent(d)
g.setParent(d)
h.setParent(e)
i.setParent(e)
j.setParent(e)
l.setParent(k)
m.setParent(k)
n.setParent(k)

tree=OntologyTree(a)

print(getFamily(a,n))
# def readFD(fdPath):
#     csv_reader=csv.reader(open(fdPath, encoding='utf-8'))
#     fdMat=[]
#     for row in csv_reader:
#         fdMat.append(row)
#     fdm=fdMat[0]
#     i=len(fdm)
#     x=[]
#     y=[]
#     fd=[x,y]
#     y.append(fdm[i-1])
#     for n in range(0,i-1):
#         x.append(fdm[n])
#     return fd
#
# fd=readFD('../data/test1FD.csv')
#
# X=fd[0]
# Y=fd[1]
#
#
# #dataMat is the test data input
#
# store=[]
# correct=[]      #store-----index-----correct
# errors=0
# for i in range(0,len(dataMat)):
#     a=[]
#     a=dataMat[i]
#     b=[]
#     for x in X:
#         b.append(a[int(x)])
#
#
#     if b not in store:
#         store.append(b)
#         correct.append(a[int(Y[0])])
#     else:
#
#         index=store.index(b)
#         guess=a[int(Y[0])]
#
#         if guess not in getFamily(root,findNodes(root,correct[index])):
#             errors=errors+1
#
# print(errors)
#
